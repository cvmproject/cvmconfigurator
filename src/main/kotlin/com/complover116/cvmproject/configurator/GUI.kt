package com.complover116.cvmproject.configurator

import java.awt.*
import javax.swing.*
import kotlin.system.exitProcess

class GUI(val project: Project, val requester: Requester, val version: String): JFrame("CVM Configurator $version") {
//    init {
//        try {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
//        } catch (e: UnsupportedLookAndFeelException) {
//            println("Native Look and Feel not supported, defaulting to Metal")
//            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName())
//        }
//    }

    private val rootPanel = JPanel()
    private val tabbedPane = JTabbedPane()

    private val configPanel: GUIConfigPanel = GUIConfigPanel(this)
    val progressDialog = ProgressDialog(this)

    private val moduleSelectPanel: GUIModuleSelectPanel;
    private val toolsPanel: GUIToolsPanel

    init {
        if (!project.isValidProject) {
            if (!askInitConfirmation()) exitProcess(0)
            val projectName = JOptionPane.showInputDialog("Please enter a name for your project") ?: exitProcess(0)
            val androidPackageName = JOptionPane.showInputDialog("Please enter an android package name for your project (Optional)")
            project.initProject(projectName, androidPackageName)
            configPanel.doVersionSelect()
        } else if (project.engineVersionInstalled == null) {
            configPanel.doSwitchVersion(project.engineVersionRequired!!)
        } else if (project.engineVersionInstalled != project.engineVersionRequired) {
            project.switchEngineVersion(project.engineVersionRequired!!, requester, progressDialog);
        }
        moduleSelectPanel = GUIModuleSelectPanel(this);

        toolsPanel = GUIToolsPanel(this)
        tabbedPane.addTab("Tools", toolsPanel)
        tabbedPane.addTab("Project Config", configPanel)
        tabbedPane.addTab("CVM Modules", moduleSelectPanel)

        rootPanel.add(tabbedPane, BorderLayout.CENTER)
//        rootPanel.add(mainProgressBar, BorderLayout.PAGE_END)
        this.add(rootPanel)
        this.pack()
        this.defaultCloseOperation = EXIT_ON_CLOSE
        this.setLocationRelativeTo(null)
        this.isVisible = true
    }

    fun askInitConfirmation(): Boolean {
        val result = JOptionPane.showConfirmDialog(
            this,
            "This directory is not a CVM Project. Initialize project here?",
            "Init project",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE
        )
        return result == JOptionPane.YES_OPTION
    }
}
