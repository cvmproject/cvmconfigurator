package com.complover116.cvmproject.configurator

import java.awt.Dimension
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Insets
import java.io.File
import java.nio.file.Path
import javax.swing.filechooser.FileFilter
import javax.swing.*

class GUIToolsPanel(val gui: GUI): JPanel(GridBagLayout()) {
    var lastTaskRunGUIConsole: GUIConsole? = null

    val desktopLabel = JLabel("Desktop")
    val androidLabel = JLabel("Android")
    val desktopRunButton = JButton("Run Desktop version")
    val desktopShowLogsButton = JButton("Show logs of last run")
    val desktopDistButton = JButton("Assemble Desktop package...")
    val androidRunButton = JButton("Run on Android via ADB")
    val androidDebugButton = JButton("Assemble Android debug apk...")
    val androidReleaseButton = JButton("Assemble Android release app bundle...")
    val androidBrokenButton = JButton("Why?")

    init {
        desktopDistButton.preferredSize = Dimension(225, 25)
        desktopRunButton.preferredSize = Dimension(225, 25)
        desktopShowLogsButton.preferredSize = Dimension(225, 15)
        androidRunButton.preferredSize = Dimension(225, 25)
        androidDebugButton.preferredSize = Dimension(225, 25)
        androidReleaseButton.preferredSize = Dimension(225, 15)

        desktopShowLogsButton.isEnabled = false

        this.add(
            desktopLabel,
            GridBagConstraints(
                0,
                0,
                2,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(10, 10, 10, 10),
                25,
                5
            )
        )
        this.add(
            desktopRunButton,
            GridBagConstraints(
                0,
                1,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(10, 10, 0, 10),
                25,
                5
            )
        )
        this.add(
            desktopDistButton,
            GridBagConstraints(
                1,
                1,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(10, 10, 0, 10),
                25,
                5
            )
        )

        this.add(
            androidRunButton,
            GridBagConstraints(
                0,
                4,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(10, 10, 0, 10),
                25,
                5
            )
        )
        this.add(
            androidDebugButton,
            GridBagConstraints(
                1,
                4,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(10, 10, 0, 10),
                25,
                5
            )
        )
        this.add(
            androidReleaseButton,
            GridBagConstraints(
                1,
                5,
                1,
                1,
                1.0,
                0.5,
                GridBagConstraints.NORTH,
                GridBagConstraints.NONE,
                Insets(0, 10, 10, 10),
                25,
                5
            )
        )

        this.add(
            desktopShowLogsButton,
            GridBagConstraints(
                0,
                2,
                1,
                1,
                1.0,
                0.5,
                GridBagConstraints.NORTH,
                GridBagConstraints.NONE,
                Insets(0, 10, 10, 10),
                25,
                5
            )
        )

        var androidLabelLength = 0
        if (Main.androidBuildToolsRoot == null) {
            androidDebugButton.isEnabled = false
            androidReleaseButton.isEnabled = false
            androidRunButton.isEnabled = false
            val message = "Android SDK not found, Android features are disabled."
            androidLabelLength = 1
            this.add(
                androidBrokenButton,
                GridBagConstraints(
                    1,
                    3,
                    1,
                    1,
                    1.0,
                    1.0,
                    GridBagConstraints.CENTER,
                    GridBagConstraints.NONE,
                    Insets(10, 10, 10, 10),
                    0,
                    0
                )
            )
            androidLabel.text = message
        }
        this.add(
            androidLabel,
            GridBagConstraints(
                0,
                3,
                androidLabelLength,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(10, 10, 10, 10),
                25,
                5
            )
        )

        androidBrokenButton.addActionListener {
            JOptionPane.showMessageDialog(gui, """
                Android SDK was not found. Please, download and install the Android SDK.
                Make sure that ANDROID_SDK_ROOT is set correctly
                Make sure that build-tools ${Main.BUILD_TOOLS_VERSION} and platform-tools are installed
            """.trimIndent())
        }

        desktopDistButton.addActionListener {
            val selectedFile = showSaveDialog("zip", ".zip - Zipped Desktop Package") ?: return@addActionListener
            val worker = object: SwingWorker<Unit, Pair<String, Int>>() {
                override fun doInBackground() {
                    gui.project.distDesktop(gui.requester, selectedFile.toPath(), gui.progressDialog)
                }

                override fun done() {
                    super.done()
                    gui.progressDialog.isVisible = false
                    JOptionPane.showMessageDialog(gui, "Desktop Package assembled!")
                }
            }
            gui.progressDialog.statusBar.text = "Assembling Desktop Package..."
            worker.execute()
            gui.progressDialog.popup()
        }

        androidDebugButton.addActionListener {
            val selectedFile = showSaveDialog("apk", ".apk - Android Package") ?: return@addActionListener
            buildAndroid(selectedFile.toPath(), false, true)
        }

        androidReleaseButton.addActionListener {
            val selectedFile = showSaveDialog("aab", ".aab - Android App Bundle") ?: return@addActionListener
            buildAndroid(selectedFile.toPath(), true, true)
        }

        desktopRunButton.addActionListener {
            if (lastTaskRunGUIConsole != null) {
                lastTaskRunGUIConsole!!.dispose()
            }

            gui.project.runDesktop(gui.requester)
        }

        desktopShowLogsButton.addActionListener {
            lastTaskRunGUIConsole!!.isVisible = true
        }

        androidRunButton.addActionListener {
            if (!buildAndroid(gui.project.buildDir.resolve("debug.apk"), false, false)) return@addActionListener
            GUIConsole("Android Console",
                listOf(
                    listOf(
                        Main.androidPlatformToolsRoot!!.resolve(if (Main.isWindows) "adb.exe" else "adb").toString(),
                        "install",
                        gui.project.buildDir.resolve("debug.apk").toString()
                    ),
                    listOf(
                        Main.androidPlatformToolsRoot!!.resolve(if (Main.isWindows) "adb.exe" else "adb").toString(),
                        "forward",
                        "tcp:20490",
                        "tcp:20483"
                    ),
                    listOf(
                        Main.androidPlatformToolsRoot!!.resolve(if (Main.isWindows) "adb.exe" else "adb").toString(),
                        "shell",
                        "am",
                        "start",
                        "-a",
                        "android.intent.action.MAIN",
                        "-n",
                        "${gui.project.androidPackageName}/com.complover116.cvmproject.cvmengine.android.AndroidLauncher"
                    ),
                    listOf(
                        "java",
                        "-jar",
                        gui.project.engineDir.resolve("CVMEngine.jar").toString(),
                        "-console_remote",
                        "localhost",
                        "-console_remote_port",
                        "20490"
                    )
                )
            ).isVisible = true
        }
    }

    private fun buildAndroid(path: Path, releaseBundle: Boolean, notification: Boolean): Boolean {
        val worker = object: SwingWorker<Boolean, Pair<String, Int>>() {
            override fun doInBackground(): Boolean {
                try {
                    gui.project.buildAndroid(gui.requester, path, releaseBundle, gui.progressDialog)
                } catch (e: Exception) {
                    SwingUtilities.invokeAndWait { JOptionPane.showMessageDialog(gui, e.message, "Android build failed!", JOptionPane.ERROR_MESSAGE) }
                    return false
                }
                if (notification)
                    SwingUtilities.invokeAndWait { JOptionPane.showMessageDialog(gui, "Android Package assembled!") }
                return true
            }

            override fun done() {
                super.done()
                gui.progressDialog.isVisible = false
            }
        }
        gui.progressDialog.statusBar.text = if (releaseBundle) "Assembling Android App Bundle..." else "Assembling Android Package..."
        worker.execute()
        gui.progressDialog.popup()
        return worker.get()
    }

    private fun showSaveDialog(extension: String, description: String): File? {
        val chooser = JFileChooser()
        chooser.addChoosableFileFilter(object: FileFilter() {
            override fun accept(f: File?): Boolean {
                return f!!.name.split(".").last() == extension || f.isDirectory
            }

            override fun getDescription(): String {
                return description
            }

        })
        chooser.isAcceptAllFileFilterUsed = false
        chooser.selectedFile = gui.project.rootDir.resolve("${gui.project.projectName}.$extension").toFile()
        val result = chooser.showDialog(gui, "Assemble")
        if (result == JFileChooser.APPROVE_OPTION) {
            return chooser.selectedFile
        }
        return null
    }
}
