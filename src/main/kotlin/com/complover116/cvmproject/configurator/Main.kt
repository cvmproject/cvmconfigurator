package com.complover116.cvmproject.configurator

import com.complover116.cvmproject.configurator.Main.Companion.BUILD_TOOLS_VERSION
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.HelpFormatter
import org.apache.commons.cli.Options
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import javax.swing.SwingUtilities

class Main {
    companion object {
        var androidBuildToolsRoot: Path? = null
        var androidPlatformToolsRoot: Path? = null
        val isWindows = System.getProperty("os.name").contains("Windows")
        const val BUILD_TOOLS_VERSION = "30.0.3"
    }
}

fun main(args: Array<String>) {
    val options = Options()
    options.addOption("r", "desktop-run", false, "Run project")
    options.addOption("d", "desktop-dist", true, "Pack project for distribution")
    options.addOption("pl", "package-linux64", false, "Pack project with JRE for distribution on linux64")
    options.addOption("pl", "package-windows64", false, "Pack project with JRE for distribution on windows64")
    options.addOption("ada", "android-debug-apk", true, "Assemble installable apk")
    options.addOption("arb", "android-release-bundle", true,"Assemble an optimized Android application bundle")
    options.addOption("elv", "engine-list-versions", false,"List all available CVMEngine versions")
    options.addOption("in", "init", false,"Initializes a CVMEngine project")
    options.addOption("ev", "engine-version", true,"CVMEngine version to use (for use with --init and --engine)")
    options.addOption("у", "engine", true,"Run CVMEngine and pass the remaining arguments to it")
    options.addOption("i", "install", false, "Copies CVMConfigurator.jar to the current directory, updating it if it exists")
    options.addOption("n", "name", true,"Set the project name")
    options.addOption("apn", "android-package-name", true,"Set the project android package name")
    options.addOption("h", "help", false, "Print the message you are now seeing")
    options.addOption("tg", "telegram-post-builds", false, "Posts all possible builds to telegram")

    val parser = DefaultParser()

    val cmd = parser.parse(options, args)

    val version = Main::class.java.classLoader.getResource("version")?.readText() ?: "UNKNOWN"

    println("CVM Configurator $version starting")
    println("Running on ${System.getProperty("os.name")}")
    val androidSDKHome = System.getenv("ANDROID_SDK_ROOT") ?: System.getenv("ANDROID_HOME")
    if (androidSDKHome == null) {
        println("Android SDK not detected, ANDROID_SDK_ROOT not set, disabling android features")
    } else {
        println("Android SDK found at $androidSDKHome")
        Main.androidBuildToolsRoot = Paths.get(androidSDKHome).resolve("build-tools").resolve(BUILD_TOOLS_VERSION)
        Main.androidPlatformToolsRoot = Paths.get(androidSDKHome).resolve("platform-tools")
        if (!Files.exists(Main.androidBuildToolsRoot!!)) {
            println("Build tools version $BUILD_TOOLS_VERSION not found, disabling android features")
            Main.androidBuildToolsRoot = null
        }
        if (!Files.exists(Main.androidPlatformToolsRoot!!)) {
            println("Platform-tools not found, disabling android features")
            Main.androidBuildToolsRoot = null
        }
    }

    val curProject = Project(Paths.get("."))

    val requester = Requester()

    val cli = CLI(curProject, requester, cmd, args)

    when {
        cmd.hasOption("help") -> {
            val formatter = HelpFormatter()
            formatter.printHelp("cvmconfigurator", options)
        }
        cmd.hasOption("telegram-post-builds") -> cli.telegramPostBuilds()
        cmd.hasOption("install") -> cli.install()
        cmd.hasOption("engine-list-versions") -> cli.listEngineVersions()
        cmd.hasOption("init") -> cli.init()
        cmd.hasOption("desktop-run") -> curProject.runDesktop(requester)
        cmd.hasOption("desktop-dist") -> curProject.distDesktop(requester, Paths.get(cmd.getOptionValue("desktop-dist")), null)
        cmd.hasOption("package-linux64") -> JVMPackager(curProject, requester).packageForPlatform("linux64")
        cmd.hasOption("package-windows64") -> JVMPackager(curProject, requester).packageForPlatform("windows64")
        cmd.hasOption("android-debug-apk") -> {
            curProject.buildAndroid(requester, Paths.get(cmd.getOptionValue("android-debug-apk")), false, null)
        }
        cmd.hasOption("android-release-bundle") -> {
            curProject.buildAndroid(requester, Paths.get(cmd.getOptionValue("android-release-bundle")), true, null)
        }
        else -> {
            SwingUtilities.invokeAndWait {
                GUI(curProject, requester, version)
            }
        }
    }
}
