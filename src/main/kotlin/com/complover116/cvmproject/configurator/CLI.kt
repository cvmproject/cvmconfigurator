package com.complover116.cvmproject.configurator

import org.apache.commons.cli.CommandLine
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime
import java.util.Date
import kotlin.system.exitProcess

class CLI(private val project: Project, val requester: Requester, private val cmd: CommandLine, val args: Array<String>) {
    private fun ask(message: String): String {
        println(message)
        print(">")
        return readLine()!!
    }

    fun install() {
        val jarPath = CLI::class.java.protectionDomain.codeSource.location.toURI().path
        println("Currently active jar is at $jarPath")
        if (Files.exists(Paths.get("CVMConfigurator.jar"))) {
            println("CVMConfigurator.jar already present in this directory, updating")
            Files.delete(Paths.get("CVMConfigurator.jar"))
        } else {
            println("Copying to CVMConfigurator.jar")
        }
        Files.copy(Paths.get(jarPath), Paths.get("CVMConfigurator.jar"))
        println("Done. You should commit the new CVMConfigurator.jar to VCS")
        println("You can now use 'java -jar CVMConfigurator.jar' instead of the 'cvmconfigurator' command, even is cvmconfigurator is not installed")
    }

    fun init() {
        val projectName = cmd.getOptionValue("name") ?: ask("Choose a name for your project")
        val androidPackageName = cmd.getOptionValue("name") ?: ask("Choose an android package name for your project")
        listEngineVersions()
        val versions = requester.getAvailableEngineVersions().map { it.version }
        var engineVersion = ""
        while (true) {
            engineVersion = cmd.getOptionValue("engine-version") ?: ask("Choose a CVMEngine version from the list above")
            if (engineVersion in versions) {
                break
            }
        }
        project.initProject(projectName, androidPackageName)
        project.switchEngineVersion(engineVersion, requester, null)
    }

    fun listEngineVersions() {
        val versions = requester.getAvailableEngineVersions()
        versions.forEach {
            if (project.isValidProject && project.engineVersionRequired == it.version) {
                println("CVMEngine ${it.version} <--- SELECTED")
            } else {
                println("CVMEngine ${it.version}")
            }
        }
    }

    fun telegramPostBuilds() {
        val apiKey = System.getenv("TG_API_KEY")
        if (apiKey == null) {
            println("TG_API_KEY environment variable not set, cannot post")
            return
        }
        val bot = TGBot(apiKey)

        val chatID = System.getenv("TG_CHAT_ID")
        if (chatID == null) {
            println("TG_CHAT_ID environment variable not set, cannot post")
            return
        }
        val desktopFileName = "${project.projectName}-${project.version}.zip"
        val androidFileName = "${project.projectName}-${project.version}-debug.apk"
        project.buildDir.toFile().mkdirs()
        project.distDesktop(requester, project.buildDir.resolve(desktopFileName), null)
        println("Posting to Telegram...")
        bot.makePost(chatID.toLong(), project.buildDir.resolve(desktopFileName).toFile(), "${project.projectName} ${project.version} (desktop) built on ${LocalDateTime.now()}")
        project.buildAndroid(requester, project.buildDir.resolve(androidFileName), false, null)
        println("Posting to Telegram...")
        bot.makePost(chatID.toLong(), project.buildDir.resolve(androidFileName).toFile(), "${project.projectName} ${project.version} (android debug APK) built on ${LocalDateTime.now()}")
        println("Done!")
        exitProcess(0)
    }
}
