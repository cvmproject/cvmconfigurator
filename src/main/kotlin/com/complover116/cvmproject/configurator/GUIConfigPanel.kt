package com.complover116.cvmproject.configurator

import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Insets
import java.util.*
import javax.swing.*

class GUIConfigPanel(val gui: GUI): JPanel(GridBagLayout()) {
    private val engineVersionLabel = JLabel()
    private val changeVersionButton = JButton("Change Engine Version...")
    private val projectNameLabel = JLabel()
    private val changeProjectNameButton = JButton("Change Project Name...")
    private val androidPackageNameLabel = JLabel()
    private val changeAndroidPackageNameButton = JButton("Change Package Name...")

    init {
        this.add(engineVersionLabel, GridBagConstraints(
                0,
                0,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(0, 0, 0, 0),
                25,
                5
            )
        )
        this.add(
            changeVersionButton,
            GridBagConstraints(
                1,
                0,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(0, 0, 0, 0),
                25,
                5
            )
        )
        this.add(projectNameLabel, GridBagConstraints(
            0,
            1,
            1,
            1,
            1.0,
            1.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.NONE,
            Insets(0, 0, 0, 0),
            25,
            5
        )
        )
        this.add(
            changeProjectNameButton,
            GridBagConstraints(
                1,
                1,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(0, 0, 0, 0),
                25,
                5
            )
        )
        this.add(androidPackageNameLabel, GridBagConstraints(
            0,
            2,
            1,
            1,
            1.0,
            1.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.NONE,
            Insets(0, 0, 0, 0),
            25,
            5
        )
        )
        this.add(
            changeAndroidPackageNameButton,
            GridBagConstraints(
                1,
                2,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.NONE,
                Insets(0, 0, 0, 0),
                25,
                5
            )
        )

        changeVersionButton.addActionListener{
            doVersionSelect()
        }

        changeProjectNameButton.addActionListener {
            val newName = JOptionPane.showInputDialog("Enter the new project name", gui.project.projectName)
            if (newName != null) {
                gui.project.projectName = newName
                updateFromProject()
            }
        }
        changeAndroidPackageNameButton.addActionListener {
            val result = JOptionPane.showConfirmDialog(
                gui,
                """WARNING: Changing the Android package name will cause Android to treat this project as a new app.
                    |DO NOT CHANGE THIS IF YOU HAVE PUBLISHED THE APP TO GOOGLE PLAY, YOU WILL LOSE THE ABILITY TO UPDATE THE APP!
                    |Are you sure you want to change the Android package name for your project?
                """.trimMargin(),
                "Confirm Android package name change",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE
            )
            if (result != JOptionPane.YES_OPTION) return@addActionListener
            val newName = JOptionPane.showInputDialog("Enter the new Android package name", gui.project.androidPackageName)
            if (newName != null) {
                gui.project.androidPackageName = newName
                updateFromProject()
            }
        }
        updateFromProject()
    }

    fun updateFromProject() {
        engineVersionLabel.text = "Installed CVMEngine Version: v${gui.project.engineVersionRequired}"
        projectNameLabel.text = "Project Name: ${gui.project.projectName}"
        androidPackageNameLabel.text = "Android Package Name: ${gui.project.androidPackageName}"
        gui.title = "CVMConfigurator ${gui.version} for ${gui.project.projectName}"
    }

    fun doSwitchVersion(selectedVersion: String) {
        val worker = object: SwingWorker<Boolean, Pair<String, Int>>() {
            override fun doInBackground(): Boolean {
                try {
                    gui.project.switchEngineVersion(selectedVersion, gui.requester, gui.progressDialog)
                } catch (e: Exception) {
                    SwingUtilities.invokeAndWait { JOptionPane.showMessageDialog(gui, e.message, "Android build failed!", JOptionPane.ERROR_MESSAGE) }
                    return false
                }
                return true
            }

            override fun done() {
                super.done()
                gui.progressDialog.isVisible = false
                if (get()) {
                    updateFromProject()
                    JOptionPane.showMessageDialog(gui, "CVMEngine v${selectedVersion} installed")
                }
            }
        }
        gui.progressDialog.statusBar.text = "Installing CVMEngine v${selectedVersion}..."
        worker.execute()
        gui.progressDialog.popup()
    }

    fun doVersionSelect(): Boolean {
        val worker = object: SwingWorker<LinkedList<Requester.GitlabPackage>, Void>() {
            override fun doInBackground(): LinkedList<Requester.GitlabPackage> {
                return gui.requester.getAvailableEngineVersions()
            }

            override fun done() {
                super.done()
                gui.progressDialog.isVisible = false
            }
        }
        gui.progressDialog.mainProgressBar.isIndeterminate = true
        gui.progressDialog.statusBar.text = "Fetching version list..."
        gui.progressDialog.mainProgressBar.string = "Connecting to Gitlab API"
        worker.execute()
        gui.progressDialog.popup()
        val versions: LinkedList<Requester.GitlabPackage> = worker.get()

        val selection: Requester.GitlabPackage? = JOptionPane.showInputDialog(
            this,
            "Select desired CVMEngine version",
            "Version Select",
            JOptionPane.PLAIN_MESSAGE,
            null,
            versions.toArray(),
            "Select..."
        ) as Requester.GitlabPackage?
        println("User selected version $selection")
        val selectedVersion = selection ?: return false
        doSwitchVersion(selectedVersion.version)
        return true
    }
}