package com.complover116.cvmproject.configurator

import java.awt.Dimension
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Insets
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.*
import kotlin.io.path.exists


class GUIModuleSelectPanel(val gui: GUI): JPanel(GridBagLayout()) {
    private val modulesAvailableModel = DefaultListModel<String>()
    private val modulesAvailableList = JList(modulesAvailableModel)
    private val modulesActiveModel = DefaultListModel<String>()
    private val modulesActiveList = JList(modulesActiveModel)
    private val moduleActivateButton = JButton("Enable >>")
    private val moduleAddButton = JButton("Add...")
    private val moduleDeactivateButton = JButton("<< Disable")
    private val moduleChangesApplyButton = JButton("Apply pending changes")

    init {
        this.add(
            modulesAvailableList,
            GridBagConstraints(
                0,
                0,
                1,
                4,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                Insets(10, 10, 10, 10),
                25,
                5
            )
        )

        modulesAvailableList.preferredSize = Dimension(200, 100)

        this.add(
            modulesActiveList,
            GridBagConstraints(
                2,
                0,
                1,
                4,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                Insets(10, 10, 10, 10),
                25,
                5
            )
        )

        modulesActiveList.preferredSize = Dimension(200, 100)

        this.add(
            moduleActivateButton,
            GridBagConstraints(
                1,
                0,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL,
                Insets(10, 10, 10, 10),
                25,
                5
            )
        )

        this.add(
            moduleAddButton,
            GridBagConstraints(
                1,
                1,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL,
                Insets(10, 10, 10, 10),
                25,
                5
            )
        )

        this.add(
            moduleDeactivateButton,
            GridBagConstraints(
                1,
                2,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL,
                Insets(10, 10, 10, 10),
                25,
                5
            )
        )

        this.add(
            moduleChangesApplyButton,
            GridBagConstraints(
                0,
                4,
                4,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                Insets(10, 10, 10, 10),
                25,
                5
            )
        )

        modulesActiveList.addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(e: MouseEvent) {
                val list = e.source as JList<*>
                if (e.clickCount == 2) {
                    val index = list.locationToIndex(e.point)
                    JOptionPane.showMessageDialog(gui, modulesActiveModel[index])
                }
            }
        })

        gui.project.enabledCVMPackages.forEach {
            modulesActiveModel.addElement(it)
        }

        gui.project.availableCVMPackages.forEach {
            if (!modulesActiveModel.contains(it))
                modulesAvailableModel.addElement(it)
        }

        updateButtonStatus()

        modulesActiveList.addListSelectionListener{
            updateButtonStatus()
        }
        modulesAvailableList.addListSelectionListener{
            updateButtonStatus()
        }

        moduleActivateButton.addActionListener {
            modulesActiveModel.addElement(modulesAvailableList.selectedValue)
            modulesAvailableModel.remove(modulesAvailableList.selectedIndex)
            moduleChangesApplyButton.isEnabled = true
        }

        moduleDeactivateButton.addActionListener {
            modulesAvailableModel.addElement(modulesActiveList.selectedValue)
            modulesActiveModel.remove(modulesActiveList.selectedIndex)
            moduleChangesApplyButton.isEnabled = true
        }

        moduleAddButton.addActionListener {
            val modulePath = JOptionPane.showInputDialog("Enter the module path relative to the project root")
            if (!gui.project.rootDir.resolve(modulePath).exists()) {
                JOptionPane.showMessageDialog(gui, "Cannot find module $modulePath, please verify that the path is correct", "Cannot find module", JOptionPane.ERROR_MESSAGE)
                return@addActionListener
            }
            modulesActiveModel.addElement("EXTERNAL:$modulePath")
            moduleChangesApplyButton.isEnabled = true
        }

        moduleChangesApplyButton.isEnabled = false
        moduleChangesApplyButton.addActionListener {
            val worker = object: SwingWorker<Unit, Unit>() {
                override fun doInBackground() {
                    gui.project.updateEngineModules(gui.progressDialog)
                }

                override fun done() {
                    super.done()
                    gui.progressDialog.isVisible = false
                }
            }
            gui.project.enabledCVMPackages = modulesActiveModel.elements().toList().toTypedArray()
            gui.progressDialog.statusBar.text = "Applying Changes..."
            worker.execute()
            gui.progressDialog.popup()
            moduleChangesApplyButton.isEnabled = false
        }
    }


    private fun updateButtonStatus() {
        moduleActivateButton.isEnabled = modulesAvailableList.selectedValue != null
        moduleDeactivateButton.isEnabled = modulesActiveList.selectedValue != null
    }
}