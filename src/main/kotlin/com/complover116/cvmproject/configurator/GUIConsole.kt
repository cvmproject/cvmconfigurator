package com.complover116.cvmproject.configurator

import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Insets
import java.io.BufferedReader
import java.io.InputStreamReader
import javax.swing.*

class GUIConsole(title: String, val commands: List<List<String>>): JFrame(title) {

    private val area = JTextArea()
    private val scrollPane: JScrollPane = JScrollPane(area)
    private val rootPanel = JPanel(GridBagLayout())
    private val killButton = JButton("Kill")
    init {
        area.columns = 100
        area.rows = 20
        area.lineWrap = false
        area.isEditable = false
        rootPanel.add(
            scrollPane,
            GridBagConstraints(
                0,
                0,
                1,
                1,
                100.0,
                100.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                Insets(0, 0, 0, 0),
                25,
                5
            )
        )
        rootPanel.add(
            killButton,
            GridBagConstraints(
                0,
                1,
                1,
                1,
                1.0,
                1.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                Insets(0, 0, 0, 0),
                25,
                5
            )
        )
        this.add(rootPanel)
        this.pack()

        val worker = object: SwingWorker<Unit, String>() {
            var process: Process? = null
            override fun doInBackground() {
                commands.forEach {
                    val builder = ProcessBuilder(it)
                    builder.redirectErrorStream(true)
                    process = builder.start()
                    val processOut = BufferedReader(InputStreamReader(process!!.inputStream))

                    var line: String?
                    while (true) {
                        line = processOut.readLine() ?: break
                        publish(line)
                    }
                    process!!.waitFor()
                }
            }

            fun killProcess() {
                process!!.destroyForcibly()
            }

            override fun process(chunks: MutableList<String>?) {
                chunks!!.forEach {
                    area.append("$it\n")
                }
            }

            override fun done() {
                super.done()
                killButton.text = "Close"
            }
        }
        killButton.addActionListener {
            worker.killProcess()
            this.isVisible = false
            this.dispose()
        }
        worker.execute()
    }
}
