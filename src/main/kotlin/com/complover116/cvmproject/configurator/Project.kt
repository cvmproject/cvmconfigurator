package com.complover116.cvmproject.configurator

import com.lordcodes.turtle.shellRun
import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.model.ZipParameters
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.nio.file.Files
import java.nio.file.Path
import java.util.*
import javax.swing.SwingUtilities
import kotlin.io.path.exists
import kotlin.system.exitProcess


fun deleteDirectory(path: Path) {
    if (!path.exists()) {
        println("Directory $path doesn't exist, not deleting")
        return
    }
    println("Deleting $path")
    Files.walk(path).use { walk ->
        walk
            .sorted(Comparator.reverseOrder())
            .forEach{
                Files.delete(it)
            }
    }
}

class Project(val rootDir : Path) {
    val cvmDir: Path = rootDir.resolve(".cvm")
    val packagesDir = cvmDir.resolve("packages")
    val engineDir = cvmDir.resolve("engine")
    val buildDir = cvmDir.resolve("build")
    private val apkBuildDir = buildDir.resolve("apk_unpacked")
    var isValidProject: Boolean
        private set
    var engineVersionRequired: String? = null
        private set
    var engineVersionInstalled: String? = null
        private set

    val version: String
        get() {
            var projectVersion = "UNKNOWN"
            val result = shellRun {
                git.gitCommand(listOf("describe", "--tags"))
            }
            if (result.matches(Regex("v?[0-9]+\\.[0-9]+\\.[0-9]+.*"))) {
                projectVersion = result
            }
            return projectVersion
        }


    var androidPackageName: String?
        set(value) {
            if (value != null)
            cvmDir.resolve("android_package_name").toFile().writeText(value)
        }
        get() {
            if (Files.exists(cvmDir.resolve("android_package_name"))) {
                return cvmDir.resolve("android_package_name").toFile().readText()
            }
            return null
        }

    var projectName: String?
        set(value) {
            if (value != null)
            cvmDir.resolve("project_name").toFile().writeText(value)
        }
        get() {
            if (Files.exists(cvmDir.resolve("project_name"))) {
                return cvmDir.resolve("project_name").toFile().readText()
            }
            return null
        }

    var enabledCVMPackages: Array<String>
        set(value) {
            cvmDir.resolve("enabled_cvm_packages").toFile().writeText(value.joinToString("\n"))
        }
        get() {
            if (Files.exists(cvmDir.resolve("enabled_cvm_packages"))) {
                return cvmDir.resolve("enabled_cvm_packages").toFile().readLines().toTypedArray()
            }
            return arrayOf("CVM_ENGINE_BASE")
        }

    val availableCVMPackages: Array<String>
        get() {
            if (!isValidProject) throw Exception("Attempted to get package list for an invalid CVM Project")
            val packageList = LinkedList<String>()
            if (Files.exists(packagesDir.resolve("unpacked").resolve("modules"))) {
                Files.list(packagesDir.resolve("unpacked").resolve("modules")).forEach {
                    if (it.toFile().isDirectory)
                        packageList.add(it.toFile().name)
                }
            }
            return packageList.toTypedArray()
        }

    init {
        isValidProject = if (!Files.exists(cvmDir)) {
            println("Not a CVM Project")
            false
        } else {
            println("CVM Directory detected")
            if (Files.exists(cvmDir.resolve("engine_version"))) {
//                engineInstalled = true;
                engineVersionRequired = cvmDir.resolve("engine_version").toFile().readText()
                println("CVM Engine version $engineVersionRequired")
                true
            } else {
                println("Engine version file not detected")
                false
            }
        }
        if (Files.exists(cvmDir.resolve("active_engine_version"))) {
            engineVersionInstalled = cvmDir.resolve("active_engine_version").toFile().readText()
        }
    }

    fun initProject(projectName: String, androidPackageName: String) {
        Files.createDirectories(cvmDir)
        cvmDir.resolve(".gitignore").toFile().writeText("""
            packages
            engine
            build
            active_engine_version
        """.trimIndent())
        this.projectName = projectName
        this.androidPackageName = androidPackageName
    }

    fun switchEngineVersion(selectedVersion: String, requester: Requester, progressDialog: ProgressDialog?) {
        Files.createDirectories(packagesDir)
        try {
            deleteDirectory(packagesDir.resolve("unpacked"))
            deleteDirectory(packagesDir.resolve("unpacked-source"))
        } catch (e: Exception) {
            println("Did not delete unpacked directory: $e")
        }
        println("Retrieving engine version v${selectedVersion}")
        println("Downloading CVMEngine.zip")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.isIndeterminate = false
                progressDialog.mainProgressBar.string = "Downloading CVMEngine.zip"
            }
        }
        requester.downloadEngineFile(selectedVersion, packagesDir, "CVMEngine.zip") { readBytes, totalBytes ->
            if (progressDialog != null) {
                SwingUtilities.invokeLater {
                    progressDialog.mainProgressBar.maximum = totalBytes.toInt()
                    progressDialog.mainProgressBar.value = readBytes.toInt()
                }
            }
        }

        println("Downloading CVMEngine-source.zip")
        if (progressDialog != null) {
            progressDialog.mainProgressBar.string = "Downloading CVMEngine-source.zip"
        }
        requester.downloadEngineFile(selectedVersion, packagesDir, "CVMEngine-source.zip") { readBytes, totalBytes ->
            if (progressDialog != null) {
                SwingUtilities.invokeLater {
                    progressDialog.mainProgressBar.maximum = totalBytes.toInt()
                    progressDialog.mainProgressBar.value = readBytes.toInt()
                }
            }
        }

        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.isIndeterminate = true
                progressDialog.mainProgressBar.string = "Unpacking CVMEngine.zip..."
            }
        }

        Files.createDirectories(packagesDir.resolve("unpacked"))

        ZipFile(packagesDir.resolve("CVMEngine.zip").toFile()).extractAll(packagesDir.resolve("unpacked").toString())

        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.isIndeterminate = true
                progressDialog.mainProgressBar.string = "Unpacking CVMEngine-source.zip..."
            }
        }

        Files.createDirectories(packagesDir.resolve("unpacked-source"))

        ZipFile(packagesDir.resolve("CVMEngine-source.zip").toFile()).extractAll(packagesDir.resolve("unpacked-source").toString())
        engineVersionRequired = selectedVersion
        cvmDir.resolve("engine_version").toFile().writeText(engineVersionRequired!!)
        updateEngineModules(progressDialog)
        println("CVMEngine v${selectedVersion} installed")
        cvmDir.resolve("active_engine_version").toFile().writeText(engineVersionRequired!!)
        engineVersionInstalled = engineVersionRequired!!
        isValidProject = true
    }

    fun updateEngineModules(progressDialog: ProgressDialog?) {
        println("Updating enabled modules")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.isIndeterminate = true
                progressDialog.mainProgressBar.string = "Updating enabled modules..."
            }
        }
        try {
            deleteDirectory(engineDir)
        } catch (e: Exception) {
            println("Did not delete engine directory: $e")
        }
        Files.createDirectories(engineDir)
        Files.copy(packagesDir.resolve("unpacked").resolve("CVMEngine.jar"), engineDir.resolve("CVMEngine.jar"))

        Files.createDirectories(engineDir.resolve("modules"))
        val unpackedModules = packagesDir.resolve("unpacked").resolve("modules")
        enabledCVMPackages.filter { !it.startsWith("EXTERNAL:") }.forEach {
            if (Files.exists(unpackedModules.resolve(it))) {
                unpackedModules.resolve(it).toFile().copyRecursively(engineDir.resolve("modules").resolve(it).toFile())
            } else {
                println("WARNING: Module $it does not exist in CVMEngine v$engineVersionRequired!")
            }
        }
    }

    fun checkForEngine(requester: Requester, progressDialog: ProgressDialog? = null): Boolean {
        if (!isValidProject) {
            println("Not a valid CVM Project. Initialize the project first with --init <cvmengine version>")
            return false
        }
        if (engineVersionInstalled != engineVersionRequired) {
            switchEngineVersion(engineVersionRequired!!, requester, null)
        }
        return true
    }

    fun runAndLogOutput(workDir: File?, command: List<String>, prefix: String, progressDialog: ProgressDialog?) {
        val builder = ProcessBuilder(command)
        if (workDir != null) {
            builder.directory(workDir)
        }
        if (engineVersionRequired != null) {
            builder.environment().set("CVM_ENGINE_VERSION", engineVersionRequired)
        }
        builder.redirectErrorStream(true)
        val engineProcess: Process = builder.start()
        val processOut = BufferedReader(InputStreamReader(engineProcess.inputStream))

        var line: String?
        while (true) {
            line = processOut.readLine() ?: break
            println("<$prefix> $line")
            if (progressDialog != null) {
                SwingUtilities.invokeLater {
                    val localLine = line
                    progressDialog.println("<$prefix> $localLine")
                }
            }
        }
        engineProcess.waitFor()
    }

    fun runDesktop(requester: Requester) {
        if (!checkForEngine(requester)) return
        println("Starting CVMEngine")
        val externalModules = enabledCVMPackages.filter { it.startsWith("EXTERNAL:") }.map { it.split(":")[1] }

        val engineCommand = mutableListOf<String>("java", "-jar", ".cvm/engine/CVMEngine.jar", "-external_modules", ".cvm/engine/modules", "-console")

        if (externalModules.isNotEmpty()) {
            println("SAGME")
            engineCommand.add("-additional_modules")
            externalModules.forEach { engineCommand.add(it) }
        } else {
            println("FAGME")
        }
        val engineProcessBuilder = ProcessBuilder(engineCommand)
        engineProcessBuilder.redirectErrorStream(true)
        engineProcessBuilder.directory(rootDir.toFile())
        val engineProcess: Process = engineProcessBuilder.start()

        val processOut = BufferedReader(InputStreamReader(engineProcess.inputStream))

        var line: String?

        val outputCopyThread = Thread({
            while (true) {
                line = processOut.readLine() ?: break
                println("<CVMENGINE> $line")
            }
            println("Output Copy Thread exited")
        }, "Output Copy Thread")
        outputCopyThread.start()

        val cvmExitMonitorThread = Thread {
            engineProcess.waitFor()
            println("CVMEngine exited!")
            processOut.close()
        }

        cvmExitMonitorThread.start()
    }

    fun distDesktop(requester: Requester, outputPath: Path, progressDialog: ProgressDialog?) {
        if (!checkForEngine(requester)) return
        println("Assembling desktop distribution..")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.isIndeterminate = true
                progressDialog.mainProgressBar.string = "Packing CVM Modules..."
            }
        }
        val outputFile = ZipFile(outputPath.toFile())
        println("Packing CVM modules...")
        val parameters2 = ZipParameters()
        parameters2.rootFolderNameInZip = "modules"
        Files.list(engineDir.resolve("modules")).forEach {
            if (Files.isDirectory(it)) {
                outputFile.addFolder(it.toFile(), parameters2)
            }
        }
        if (Files.exists(rootDir.resolve("modules"))) {
            println("Packing project modules...")
            if (progressDialog != null) {
                SwingUtilities.invokeLater {
                    progressDialog.mainProgressBar.string = "Packing project modules..."
                }
            }
            Files.list(rootDir.resolve("modules")).forEach {
                if (Files.isDirectory(it)) {
                    outputFile.addFolder(it.toFile(), parameters2)
                }
            }
        }
        val externalModules = enabledCVMPackages.filter { it.startsWith("EXTERNAL:") }.map { it.split(":")[1] }
        if (externalModules.isNotEmpty()) {
            println("Packing external modules...")
            externalModules.forEach {
                outputFile.addFolder(File(it), parameters2)
            }
        }
        println("Packing CVMEngine.jar...")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.string = "Packing CVMEngine.jar..."
            }
        }
        val parameters = ZipParameters()
        parameters.fileNameInZip = "CVMEngine.jar"
        outputFile.addFile(engineDir.resolve("CVMEngine.jar").toFile(), parameters)
        println("Done assembling desktop distribution!")
    }

    fun buildAndroid(requester: Requester, outputPath: Path, releaseBundle: Boolean, progressDialog: ProgressDialog?) {
        if (Main.androidBuildToolsRoot == null) {
            println("Cannot build android package: cannot find android build-tools ${Main.BUILD_TOOLS_VERSION}")
            exitProcess(-1)
        }
        val androidPackageName = this.androidPackageName
        val projectName = this.projectName
        if (androidPackageName == null) {
            println("Cannot build android package: android package name not set")
            exitProcess(-1)
        }
        if (projectName == null) {
            println("Cannot build android package: project name not set")
            exitProcess(-1)
        }
        if (!checkForEngine(requester)) return
        println("Assembling android distribution...")
        if (Files.exists(apkBuildDir)) {
            deleteDirectory(apkBuildDir)
        }
        Files.createDirectories(apkBuildDir)

        println("Preparing build environment...")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.string = "Preparing build environment..."
                progressDialog.println("Preparing build environment...")
            }
        }

        Files.list(packagesDir.resolve("unpacked-source")).forEach {
            if (Files.isDirectory(it)) {
                it.toFile().copyRecursively(apkBuildDir.resolve(it.fileName).toFile(), overwrite = true)
            } else {
                it.toFile().copyTo(apkBuildDir.resolve(it.fileName).toFile(), overwrite = true)
            }
        }

        println("Wiping modules dir...")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.string = "Wiping modules dir..."
                progressDialog.println("Wiping modules dir...")
            }
        }

        deleteDirectory(apkBuildDir.resolve("android/assets/modules"))
        Files.createDirectories(apkBuildDir.resolve("android/assets/modules"))

        println("Copying engine modules...")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.string = "Copying engine modules..."
                progressDialog.println("Copying engine modules...")
            }
        }
        Files.list(engineDir.resolve("modules")).forEach {
            if (Files.isDirectory(it)) {
                it.toFile().copyRecursively(apkBuildDir.resolve("android/assets/modules").resolve(it.fileName).toFile(), overwrite = true)
            }
        }

        println("Copying project modules...")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.string = "Copying project modules..."
                progressDialog.println("Copying project modules...")
            }
        }
        if (Files.exists(rootDir.resolve("modules"))) {
            Files.list(rootDir.resolve("modules")).forEach {
                if (Files.isDirectory(it)) {
                    it.toFile().copyRecursively(apkBuildDir.resolve("android/assets/modules").resolve(it.fileName).toFile(), overwrite = true)
                }
            }
        }

        if (Files.exists(cvmDir.resolve("icon.png"))) {
            apkBuildDir.resolve("android/res/drawable-mdpi/ic_launcher_foreground.png").toFile().delete()
            cvmDir.resolve("icon.png").toFile().copyTo(apkBuildDir.resolve("android/res/drawable-mdpi/ic_launcher_foreground.png").toFile())
        }

        println("Patching AndroidManifest.xml...")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.string = "Patching manifest..."
                progressDialog.println("Patching manifest...")
            }
        }
        var androidManifest = apkBuildDir.resolve("android/AndroidManifest.xml").toFile().readText()
        androidManifest = androidManifest.replace("com.complover116.cvmproject.cvmengine.test", androidPackageName)
        androidManifest = androidManifest.replace("@string/app_name", projectName)
        apkBuildDir.resolve("android/AndroidManifest.xml").toFile().writeText(androidManifest)

        println("Patching build.gradle...")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.string = "Patching build.gradle..."
                progressDialog.println("Patching build.gradle...")
            }
        }
        var androidBuildGradle = apkBuildDir.resolve("android/build.gradle").toFile().readText()
        androidBuildGradle = androidBuildGradle.replace("com.complover116.cvmproject.cvmengine.test", androidPackageName)
        apkBuildDir.resolve("android/build.gradle").toFile().writeText(androidBuildGradle)


        val buildingMessage = if (releaseBundle) "Building release bundle..." else "Building debug apk..."
        println(buildingMessage)
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.string = buildingMessage
                progressDialog.mainProgressBar.isIndeterminate = true
            }
        }
        if (!Main.isWindows) {
            runAndLogOutput(
                apkBuildDir.toFile(),
                listOf(
                    "chmod", "+x", "gradlew"
                ),
                "BUILD",
                progressDialog
            )
        }

        runAndLogOutput(
            apkBuildDir.toFile(),
            listOf(
                apkBuildDir.resolve(if (Main.isWindows) "gradlew.bat" else "gradlew").toAbsolutePath().toString(),
                if (releaseBundle) "android:bundleRelease" else "android:assembleDebug"
            ),
            "BUILD",
            progressDialog
        )

        println("Copying to output...")
        if (progressDialog != null) {
            SwingUtilities.invokeLater {
                progressDialog.mainProgressBar.string = "Copying to output..."
                progressDialog.mainProgressBar.isIndeterminate = true
            }
        }
        val buildResultPath = if (releaseBundle) "android/build/outputs/bundle/debug/android-release.aab" else "android/build/outputs/apk/debug/android-debug.apk"
        apkBuildDir.resolve(buildResultPath).toFile().copyTo(outputPath.toFile(), overwrite = true);
        println("Done assembling android distribution")
    }
}
