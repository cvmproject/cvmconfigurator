package com.complover116.cvmproject.configurator

import com.badlogicgames.packr.Packr
import com.badlogicgames.packr.PackrConfig
import net.lingala.zip4j.ZipFile
import kotlin.io.path.absolute
import kotlin.io.path.createDirectories
import kotlin.io.path.deleteIfExists


class JVMPackager(private val project: Project, private val requester: Requester, private val progressDialog: ProgressDialog? = null) {
    fun packageForPlatform(platform: String) {
        println("Preparing to package for $platform - building multiplatform zip")
        deleteDirectory(project.buildDir.resolve("package-$platform"))
        deleteDirectory(project.buildDir.resolve("multiplatform-zip-unpacked"))
        project.buildDir.resolve("package-$platform").createDirectories()
        project.buildDir.resolve("jre-$platform").createDirectories()

        project.distDesktop(requester, project.buildDir.resolve("dist.zip"), null)
        println("Unpacking multiplatform zip...")
        ZipFile(project.buildDir.resolve("dist.zip").toFile()).extractAll(project.buildDir.resolve("multiplatform-zip-unpacked").toString())
        println("Packaging for $platform using packr")
        val config = PackrConfig()
        if (platform == "windows64") {
            config.platform = PackrConfig.Platform.Windows64
            config.jdk = "https://github.com/adoptium/temurin11-binaries/releases/download/jdk-11.0.22%2B7/OpenJDK11U-jre_x64_windows_hotspot_11.0.22_7.zip"
        } else {
            config.platform = PackrConfig.Platform.Linux64
            config.jdk = "https://github.com/adoptium/temurin11-binaries/releases/download/jdk-11.0.22%2B7/OpenJDK11U-jre_x64_linux_hotspot_11.0.22_7.tar.gz"
        }
        config.executable = project.projectName
        config.classpath = listOf(project.buildDir.resolve("multiplatform-zip-unpacked").resolve("CVMEngine.jar").absolute().toString())
        config.removePlatformLibs = config.classpath
        config.mainClass = "com.complover116.cvmproject.cvmengine.desktop.DesktopLauncher"
        config.vmArgs = listOf("Xmx1G")
        config.minimizeJre = "soft"
//        config
        config.cacheJre = project.buildDir.resolve("jre-$platform").toFile()
        config.outDir = project.buildDir.resolve("package-$platform").toFile()

        val packr = Packr()
        packr.pack(config)
        println("Copying other files...")
        project.buildDir.resolve("multiplatform-zip-unpacked").resolve("CVMEngine.jar").deleteIfExists()
        project.buildDir.resolve("multiplatform-zip-unpacked").toFile().copyRecursively(project.buildDir.resolve("package-$platform").toFile())
        val executableName = project.projectName + if (platform == "windows64") ".exe" else ""
        project.buildDir.resolve("package-$platform/.itch.toml").toFile().writeText("""
            [[actions]]
            name = "Play"
            path = "$executableName"

            [[actions]]
            name = "Launch with CVMEngine console"
            path = "$executableName"
            args = ["--console"]
        """.trimIndent())
        println("Done! You can find the output in ${project.buildDir.resolve("package-$platform")}")
    }
}
