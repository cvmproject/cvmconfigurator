package com.complover116.cvmproject.configurator

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.ProgressCallback
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.json.responseJson
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.nio.file.Path
import java.util.*

val packageRegistryToken: String = "jtCeiBPuR9ifva4M2wBd"
val packageRegistryUser: String = "CVMConfigurator"

class GitlabAPIException(message: String): Exception(message)

class Requester {
    data class GitlabPackage(val version: String, val id: Int) {
        override fun toString(): String {
            return "CVMEngine v$version"
        }
    }

    fun getAvailableEngineVersions(): LinkedList<GitlabPackage> {
        println("Fetching engine version list")
        val response = Fuel.get("https://gitlab.com/api/v4/projects/7538188/packages?per_page=100&sort=desc").responseJson()
        val packages: JSONArray = response.third.component1()?.array() ?: throw GitlabAPIException("Cannot communicate with Gitlab API")

        val versions: LinkedList<GitlabPackage> = LinkedList()

        for (pkg in packages) {
            if (pkg is JSONObject) {
                versions.add(GitlabPackage(
                    pkg.getString("version") ?: throw GitlabAPIException("Invalid response from Gitlab"),
                    pkg.getInt("id")
                ))
            }
        }
        println("Fetched ${versions.size} versions")
        return versions
    }

    fun downloadEngineFile(version: String, downloadPath: Path, filename: String, progressCallback: ProgressCallback) {
        Fuel.download("https://gitlab.com/api/v4/projects/7538188/packages/generic/CVMEngine/${version}/$filename").fileDestination {
                _, _ -> downloadPath.resolve(filename).toFile()
        }.progress(progressCallback).authentication().basic(packageRegistryUser, packageRegistryToken).response()
    }
}
