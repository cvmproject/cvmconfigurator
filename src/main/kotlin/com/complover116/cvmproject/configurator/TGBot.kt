package com.complover116.cvmproject.configurator
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.TelegramFile
import java.io.File

class TGBot(private val apiKey: String) {
    private val botInstance = bot {
        token = apiKey
    }

    fun makePost(chatID: Long, file: File, message: String) {
        botInstance.sendDocument(ChatId.fromId(chatID), TelegramFile.ByFile(file), message)
    }

    fun shutDown() {
        botInstance.stopPolling()
    }
}

