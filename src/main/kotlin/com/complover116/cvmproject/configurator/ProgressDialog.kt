package com.complover116.cvmproject.configurator

import java.awt.Dimension
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Insets
import javax.swing.*

class ProgressDialog(parent: JFrame): JDialog(parent, true) {
    val mainProgressBar = JProgressBar()
    val statusBar = JLabel("Loading...")
    private val area = JTextArea()
    private val scrollPane: JScrollPane = JScrollPane(area)
    private val rootPanel = JPanel(GridBagLayout())

    fun println(str: String) {
        area.append("$str\n")
    }

    init {
        this.defaultCloseOperation = DO_NOTHING_ON_CLOSE
        mainProgressBar.preferredSize = Dimension(200, 20)
        mainProgressBar.isStringPainted = true
        area.columns = 100
        area.rows = 10
        area.lineWrap = false
        area.isEditable = false
        rootPanel.add(
            scrollPane,
            GridBagConstraints(
                0,
                0,
                2,
                1,
                100.0,
                100.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                Insets(0, 0, 0, 0),
                25,
                5
            )
        )
        rootPanel.add(
            statusBar,
            GridBagConstraints(
                0,
                1,
                1,
                1,
                100.0,
                100.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                Insets(0, 0, 0, 0),
                25,
                5
            )
        )
        rootPanel.add(
            mainProgressBar,
            GridBagConstraints(
                1,
                1,
                1,
                1,
                100.0,
                100.0,
                GridBagConstraints.CENTER,
                GridBagConstraints.BOTH,
                Insets(0, 0, 0, 0),
                25,
                5
            )
        )
        this.contentPane = rootPanel
        this.isResizable = false
        this.title = "Progress"
        this.setLocationRelativeTo(parent)
        this.pack()
    }

    fun popup() {
        this.pack()
        this.setLocationRelativeTo(parent)
        this.isVisible = true
        this.area.text = ""
    }
}