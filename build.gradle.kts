import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "2.0.0"
    id("com.palantir.git-version") version "3.0.0"
}

repositories {
    mavenCentral()
    maven(uri("https://artifactory.nimblygames.com/artifactory/ng-public-release/"))
    maven(uri("https://jitpack.io"))
}

buildscript {
    repositories {
        google()
    }
}

dependencies {
    implementation("com.github.kittinunf.fuel:fuel:2.3.1")
    implementation("com.github.kittinunf.fuel:fuel-json:2.3.1")
    // https://mvnrepository.com/artifact/org.json/json
    implementation("org.json:json:20231013")
    // https://mvnrepository.com/artifact/net.lingala.zip4j/zip4j
    implementation("net.lingala.zip4j:zip4j:2.11.5")
    implementation("commons-cli:commons-cli:1.5.0")
    implementation("com.badlogicgames.packr:packr:3.0.3")
    implementation("io.github.kotlin-telegram-bot.kotlin-telegram-bot:telegram:6.1.0")
    implementation("com.lordcodes.turtle:turtle:0.8.0")
}


group = "com.complover116.cvmproject.configurator"
val gitVersion: groovy.lang.Closure<String> by extra
version = gitVersion()


tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.jar {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes(mapOf("Implementation-Title" to project.name,
        "Main-Class" to "com.complover116.cvmproject.configurator.MainKt"
        ))
    }
    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })
}

tasks.register("createProperties") {
    dependsOn(tasks.processResources)
    doLast {
        File("$projectDir/src/main/resources/version").writeText(project.version.toString())
    }
}

tasks.classes {
    dependsOn("createProperties")
}
